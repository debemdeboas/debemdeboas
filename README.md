<div align="center">

# Welcome!

</div>

Hi! My name is Rafael and I'm a Computer Scientist student currently based in Porto Alegre, Brazil. I aim to develop elegant solutions for complex problems. I'm currently working as a software engineer and R&D analyst.

### My links

<a href="https://gitlab.com/debemdeboas">
    <img src="https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white">
</a>

<a href="https://www.linkedin.com/in/rbem/">
    <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white">
</a>

<a href="https://stackoverflow.com/users/9918829/rafa-de-boas">
    <img src="https://img.shields.io/badge/Stack_Overflow-FE7A16?style=for-the-badge&logo=stack-overflow&logoColor=white">
</a>

<a href="https://www.instagram.com/debemdeboas/">
    <img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white">
</a>
